FROM php:7.3.25

RUN pecl install xdebug-2.9.0
RUN echo "zend_extension=$(find /usr/local/lib/php/extensions/ -name xdebug.so)" > /usr/local/etc/php/conf.d/xdebug.ini
RUN echo "xdebug.mode=coverage" >> /usr/local/etc/php/conf.d/xdebug.ini
RUN apt-get update && apt-get install -y unzip git
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
